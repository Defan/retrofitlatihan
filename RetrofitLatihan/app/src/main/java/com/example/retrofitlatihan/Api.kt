package com.example.retrofitlatihan

import android.telecom.Call
import retrofit2.http.GET

interface Api{
@GET(value "posts")
fun getPosts (): Call<ArrayList<PostResponse>>
}